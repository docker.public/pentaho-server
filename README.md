# Pentaho 8.1 Docker Image
###### Based on work done by Jonathan DeMarks (https://github.com/ca0abinary/docker-pentaho)

## Clone Repository and Build Image

Clone git repository https://gitlab.com/docker.public/pentaho-server.git

```
git clone https://gitlab.com/docker.public/pentaho-server.git
```

```
cd pentaho-server/
docker build -t americatelperu/docker-pentaho:8.1.0.0-365 .
```

## Run Pentaho Server
```
docker-compose up -d
```